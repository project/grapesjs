GrapesJS
GrapesJS is a multi-purpose page builder which combines different plugins 
and intuitive drag and drop interface with the goal to help your clients take 
control of content creation and display. A perfect solution to create beautiful 
pages without any knowledge of coding.

To Install & Configure:
1. Place module in /sites/all/modules/contrib folder or please check the url 
for [module install] (https://www.drupal.org/docs/7/extend/installing-modules)
2. Download [GrapesJs](https://github.com/artf/grapesjs) and place it in the 
/sites/all/libraries/ director and name the folder grapesjs.
3. Download [CKEditor 4](https://ckeditor.com/ckeditor-4/download/) and place 
it in the /sites/all/libraries/ directory and name it ckeditor.
4. Install module from the modules page.
5. Currently set to just work a text format with the machine name 
"visual_editor", 
so you will need to create a new text format from the configs page 
/admin/config/content/formats/add and name it "Visual Editor" with the 
machine name of "visual_editor".

To use:
1. Go to an entity with text field and select the format you set GrapesJS 
to as the Text Editor "Visual Editor".
2. Click the "Open Editor" Button and enjoy!
